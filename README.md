
# WOW, git!

This repo is meant to serve as a guide for advanced, obscure, or 
downright weird operations that one may find useful while utilizing 
the ubiquitous git decentralized versioning software. For more common 
items, try [OhShitGit](https://ohshitgit.com/) or the 
[official git documentation](https://git-scm.com/docs/giteveryday).

For more in-depth stuff, scour the documentation for weirdness like 
I did, or [Duck it](https://start.duckduckgo.com), or read the 
manpages.


## modes of obscurity:

[Patchy Versioning](patchy_versioning.md)

