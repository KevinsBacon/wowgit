# Patchy Versioning

***(a.k.a. Dealing With Two Conflicting Canonical Timelines, far,***
***far better than Disney did)***

*(a.k.a. How To Hire Dave Filoni To Patch Your Disparate*
*Continuities)*

## Motivation

So imagine a world in which you have two separate branches being 
maintained with their own individual histories such that both 
branches have commits that must not be merged into the other. Suppose 
further that no other branch exists that both (1) has the necessary 
commonalities required for further local development AND (2) does not 
have any commits that would contaminate either branch should it be 
merged with either. Essentially, the project has two canonical 
verions: two main branches that conflict. This could even apply to 
two separate repositories.

If one were to create a new branch off of either of these with the 
intent of eventually merging the new commits into both branches, 
the branch would still contaminate the other branch once its history 
was merged. One could `git cherry-pick`, but honestly I cannot be 
bothered to learn how to use that tool, plus there is an alternative 
way that can be more flexible, allowing for more user intervention in 
case certain parts of a commit must be reviewed at length, edited, 
etc. This way is borrowed from mail list devs who still share patches 
the old-school way for some of these same reasons.

Enter patchy versioning.


## How to

Given two canonical branches (A and B) and a new feature or bug 
branch (C) that is based off of B:


### Create the patch

While HEAD is on branch C:

----------------

#### case: the changes are not yet committed

First stage the changes that apply to the ticket, avoiding any that 
do not.

Then create a patch file for the changes you made to your working 
file tree:

```bash
git diff --staged -p --stat > my_feature.patch
```

You can then `git commit` as usual to apply your staged changes to C, 
and a record of those changes is now stored in my_feature.patch.

#### case: the changes are already committed to branch C in one or more commits

Create a patch file for the changes you made to C:

```bash
git format-patch -1 --stdout > my_feature.patch
# or -<number of commits since A> or <commit0>..<commit1> instead of -1
```

----------------

This patch details the changes you are applying to feature or bugfix C, 
and it can be used to implement similar changes to any version of the 
software, including into a branch D for merge into A, whether 
automatically with or without edits, and it can also be kept after 
applying its changes as a convenient reference in case of any issues.

If the file is kept anywhere in the working directory tree, be sure 
to add `*.patch` to the .gitignore file and of course don't ever 
commit it directly because that's tacky.

By now, you are safe to submit a pull request to pull C into B 
pending any final commits/tweaks.


### Apply the patch

While HEAD is on branch D based off of A (I'm assuming you know how 
to get there cleanly using `git checkout` and `git stash` with or 
without the options `-b` or `pop` as needed), apply the patch with:

```bash
git apply --check my_feature.patch  # ensure the patch is relevant to this branch
git apply my_feature.patch
```

This applies the patch to your working directory but does not commit 
the changes to the branch. This allows you to then test the app and 
verify everything is good to go before commiting the changes.

If instead you want to apply and commit in one fell swoop, run:

```bash
git am --signoff < my_feature.patch
```

By now, you are safe to submit a pull request for D into A!
